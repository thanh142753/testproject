package Utils;

import net.serenitybdd.core.webdriver.driverproviders.DriverProvider;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;

public class CommonUtils {

    public static String getRandomStringNumber(int length) {
        String generatedString = RandomStringUtils.randomNumeric(length);
//        System.out.println(generatedString);
        return generatedString;
    }

    public static String getRandomString(int length) {
        String generatedString = RandomStringUtils.randomAlphabetic(length);
        return generatedString;
    }


}
