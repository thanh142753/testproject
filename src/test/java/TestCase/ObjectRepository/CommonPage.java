package TestCase.ObjectRepository;

import TestCase.Widget.CommonPageWidget;
import net.serenitybdd.core.pages.PageObject;


public class CommonPage extends PageObject {

    public CommonPageWidget commonPageWidget;

    public CommonPageWidget getCommonPageWidget() {
        return commonPageWidget;
    }

}
