package TestCase.Stepdefinitions;

import TestCase.TestSteps.CommonSteps;
import io.cucumber.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class MyStepdefs {
    @Steps
    CommonSteps commonSteps;

    @Given("I go to gg and input text")
    public void iGoToGgAndInputText() {
        commonSteps.openLoginPage();
        commonSteps.goToGG();
    }
}
