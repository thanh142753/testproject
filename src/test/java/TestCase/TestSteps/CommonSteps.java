package TestCase.TestSteps;

import TestCase.ObjectRepository.CommonPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class CommonSteps {

    CommonPage commonPage;

    @Step
    public void goToGG() {
        commonPage.getCommonPageWidget().goToGG();
    }

    @Step
    public void openLoginPage() {
        commonPage.open();
    }
}
