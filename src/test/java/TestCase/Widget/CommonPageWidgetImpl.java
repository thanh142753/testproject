package TestCase.Widget;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WidgetObjectImpl;
import org.openqa.selenium.*;
import org.openqa.selenium.support.pagefactory.ElementLocator;

public class CommonPageWidgetImpl extends WidgetObjectImpl implements CommonPageWidget {
    public CommonPageWidgetImpl(PageObject page, ElementLocator locator, WebElement webElement, long timeoutInMilliseconds) {
        super(page, locator, webElement, timeoutInMilliseconds);
    }

    @Override
    public void goToGG() {
        try {
            WebElement webElement = getPage().getDriver().findElement(By.xpath("//input[@name='q']"));
            webElement.sendKeys("abc");
            System.out.println("abc");
//            Thread.sleep(10000);
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
