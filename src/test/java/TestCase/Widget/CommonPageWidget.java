package TestCase.Widget;

import net.serenitybdd.core.annotations.ImplementedBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WidgetObject;
import org.openqa.selenium.WebElement;

@ImplementedBy(CommonPageWidgetImpl.class)
public interface CommonPageWidget extends WidgetObject {

    void goToGG();
}
