package TestSuite;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features/ebxUi/example.feature",
        glue = {"TestCase"},
        tags = "@Smoke"
//        search_by_keyword.feature, example.feature
)
public class CucumberTestSuite {}
